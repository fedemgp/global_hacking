#include <stdio.h>
#include <stdlib.h>

#include "awesome_secure_library.h"

int main(int argc, char const *argv[]) {
	size_t secret_key;
	printf("Introduzca la contraseña secreta: ");
	scanf("%lu", &secret_key);
	if (evaluate_key(secret_key)) {
		printf("Ganaste!!\n");
	}
	return 0;
}
