# Global hacking


La idea de este proyecto es un simple ejercicio donde nos vestiremos de hackers para encontrar la contraseña secreta y ganar la partida.
Sin usar fuerza bruta, ¿cual es la clave secreta? ¿cómo se podría arreglar el problema de la librería `secure_library.a`? 
Pista: este ejemplo busca explicar por qué el uso de las funciones globales son un peligro.

![](assets/hackerman.jpg)
