CFLAGS := -Wall -Werror -pedantic -std=c99

secure: main.o secure_library.a
	gcc $^ -o $@

main.o: main.c awesome_secure_library.h
	gcc -c $(CFLAGS) -c $< -o $@

clean:
	rm -f *.o secure
