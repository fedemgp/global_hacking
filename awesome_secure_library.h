#ifndef __AWESOME_SECURE_LIBRARY_H__
#define __AWESOME_SECURE_LIBRARY_H__

#include <stdbool.h>
#include <stdlib.h>

bool evaluate_key(size_t secret_key);
#endif
